/*
* Sample of using [StatexWidget] for series of use cases.
* */
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared/shared.dart';

import 'apps/home/home.dart';
import 'apps/home/src/home_sub_page.dart';
import 'apps/home/src/sub_router_dialog.dart';
import 'apps/main/main.dart';

void main() {
  // Dependency Inversion Principle: find realization through interface.
  // `fenix` is for recreate controller from scratch after disposing.
  // (if you need to stay instance in memory use `markAsPermanent` instead)
  Get.lazyPut<HomeSubPageController>(
    () => HomeSubPageControllerImpl(),
    tag: HomeSubPageController.someTagForFindStrategy,
    fenix: true,
  );
  Get.lazyPut<HomePageController>(
    () => HomePageControllerImpl(),
    fenix: true,
  );
  //~
  runApp(GetLifeCycleControlApp());
}

class GetLifeCycleControlApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'GetLifeCycleControl Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainPage(),
    );
  }
}

////////////////////////////////////////////////////////////////////////////////

/// Implementation of [HomeSubPageController] for illustrate
/// Dependency Inversion Principle.
class HomeSubPageControllerImpl extends HomeSubPageController {

  final _name$ = 'Ville'.obs;

  @override
  String get name$ => _name$.value;

  @override
  set name$(String value) {
    if(value.isNotEmpty){
      _name$(value);
    }
  }

  @override
  void toHomePage() {
    Get.bottomSheet(SubRouterDialog(
      'Go back to Home',
      okCallback: () {
        Get.back();
        Get.back();
      },
      cancelCallback: () {
        Get.back();
      },
      color: Colors.green,
    ));
  }

  @override
  void onWidgetInitState() {
    super.onWidgetInitState();
    debugPrint('$now: HomeSubPageControllerImpl.onWidgetInitState');
  }

  @override
  void onWidgetDisposed() {
    debugPrint('$now: HomeSubPageControllerImpl.onWidgetDisposed');
    super.onWidgetDisposed();
  }

  @override
  void onInit() {
    super.onInit();
    debugPrint('$now: HomeSubPageControllerImpl.onInit');
  }

  @override
  void onReady() {
    super.onReady();
    debugPrint('$now: HomeSubPageControllerImpl.onReady');
  }

  @override
  void onClose() {
    debugPrint('$now: HomeSubPageControllerImpl.onClose');
    super.onClose();
  }
}

