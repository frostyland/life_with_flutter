import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:with_getx/with_getx.dart';

enum Tab { home, business }

class MainPageController extends StatexController {
  final selectedTab$ = Tab.home.obs;

  final pageController = PageController();

  void toPage(int index) {
    selectedTab$(Tab.values.elementAt(index));
    Future.delayed(Duration.zero).then((_) {
      pageController.animateToPage(
        index,
        curve: Curves.ease,
        duration: const Duration(milliseconds: 500),
      );
    });
  }
}
