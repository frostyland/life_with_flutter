import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_controller_lifecycle/apps/business/business.dart';
import 'package:get_controller_lifecycle/apps/home/home.dart';
import 'package:shared/shared.dart';
import 'package:with_getx/with_getx.dart';

import 'main_page_controller.dart';

class MainPage extends StatexWidget<MainPageController> {
  MainPage() : super(() => MainPageController());

  @override
  Widget buildWidget(BuildContext context) {
    debugPrint('$now: MainPage.buildWidget');
    return Scaffold(
      body: PageView(
        controller: controller.pageController,
        children: [
          HomePage(),
          BusinessPage(),
        ],
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.business),
              label: 'Business',
            ),
          ],
          currentIndex: controller.selectedTab$().index,
          selectedItemColor: Colors.blue,
          onTap: controller.toPage,
        ),
      ),
    );
  }
}
