import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_controller_lifecycle/main.dart';
import 'package:shared/shared.dart';
import 'package:with_getx/with_getx.dart';

/// [HomeSubPage] links to [HomeSubPageControllerImpl] via interface
/// by using `super.lazyAdjust`.
/// To revoke factory after destroying instance we use `fenix = true`
/// when define `Get.lazyPut` earlier.
class HomeSubPage extends StatexWidget<HomeSubPageController> {
  HomeSubPage({Key? key})
      : super.lazyAdjust(
          key: key,
          // When we make controller lazily, we need pass here
          // correct tag to link it with correct instance in manager
          tag: HomeSubPageController.someTagForFindStrategy,
        );

  /// This allows to use const with widget constructor.
  @override
  Widget buildWidget(BuildContext context) => _Inner();
}

/// This allows to use const with widget constructor.
class _Inner extends GetViewSim<HomeSubPageController> {
  const _Inner({Key? key})
      : super(tag: HomeSubPageController.someTagForFindStrategy, key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Material(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Obx(() => Text(
                    controller._title(),
                    style: Theme.of(context).textTheme.headline5,
                  )),
            ),
            ElevatedButton(
              onPressed: controller.toHomePage,
              child: Text('Go back to Home?'),
            ),
            Obx(
              () => Checkbox(
                value: controller.flag$,
                onChanged: (_) {
                  controller.toggle();
                  controller.name$ = '$now';
                },
              ),
            ),
            Obx(() => Text(controller.name$)),
          ],
        ),
      ),
    );
  }
}

///
abstract class HomeSubPageController extends StatexController {
  static const someTagForFindStrategy = 'TAG';

  final _title = 'Home Sub Page'.obs;

  get title => _title.value;

  final _flag = false.obs;

  get flag$ => _flag.value;

  void toggle() => _flag.toggle();

  // How to use abstract reactive variable with override in descendant.

  String get name$;

  set name$(String value);

  //~ How to use abstract reactive variable with override in descendant.

  ///
  void toHomePage();
}
