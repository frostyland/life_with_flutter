import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared/shared.dart';
import 'package:with_getx/with_getx.dart';

import 'home_sub_page.dart';
import 'sub_router_dialog.dart';

/// [HomePage] links to [HomePageControllerImpl] via interface
/// by using `super.lazyAdjust`.
/// To revoke factory after destroying instance we use `fenix = true`
/// when define `Get.lazyPut` earlier.
/// Here we use `markAsPermanent` - it emulates **permanent** for
/// case of `super.lazyAdjust`
class HomePage extends StatexWidget<HomePageController> {
  HomePage({Key? key})
      : super.lazyAdjust(
          markAsPermanent: true,
          key: key,
        ) {
    debugPrint('$now: HomePage.HomePage');
  }

  /// This allows to use const with widget constructor.
  @override
  Widget buildWidget(BuildContext context) => _Inner();
}

/// This allows to use const with widget constructor.
class _Inner extends GetViewSim<HomePageController> {
  const _Inner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Obx(() => Text(
                controller.title$,
                style: Theme.of(context).textTheme.headline5,
              )),
          ElevatedButton(
            onPressed: controller.toSubPage,
            child: Text('Go to Sub?'),
          ),
          16.h,
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Timer DOES NOT reinitialize\n'
              'because controller is PERMANENT',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
          Obx(() => Text(
                '${controller.timerTitle$}',
                style: Theme.of(context).textTheme.headline4,
              )),
        ],
      ),
    );
  }
}

/// Implementation for [HomePageController]
class HomePageControllerImpl extends HomePageController {
  HomePageControllerImpl() {
    debugPrint('$now: HomePageControllerImpl.HomePageControllerImpl');
  }

  @override
  void toSubPage() {
    Get.bottomSheet(SubRouterDialog(
      'Go to Sub',
      okCallback: () {
        Get.back();
        Get.to(() => HomeSubPage());
      },
      cancelCallback: () {
        Get.back();
      },
    ));
  }

  @override
  void onInit() {
    super.onInit();
    debugPrint('$now: HomePageControllerImpl.onInit');
  }

  @override
  void onReady() {
    super.onReady();
    debugPrint('$now: HomePageControllerImpl.onReady');
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      _timerTitle.value++;
    });
  }

  @override
  void onClose() {
    debugPrint('$now: HomePageControllerImpl.onClose');
    _timer.cancel();
    super.onClose();
  }

  @override
  void onWidgetInitState() {
    super.onWidgetInitState();
    debugPrint('$now: HomePageControllerImpl.onWidgetInitState');
  }

  @override
  void onWidgetDisposed() {
    debugPrint('$now: HomePageControllerImpl.onWidgetDisposed');
    super.onWidgetDisposed();
  }
}

abstract class HomePageController extends StatexController {
  final _title = 'Home Page'.obs;

  get title$ => _title.value;

  final _timerTitle = 1.obs;

  get timerTitle$ => _timerTitle.value;

  late Timer _timer;

  void toSubPage();
}
