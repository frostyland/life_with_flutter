import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared/shared.dart';
import 'package:with_getx/with_getx.dart';

/// [SubRouterDialog] should be called with various parameters
/// for various use cases.
/// This becomes possible by `Statex*`.
class SubRouterDialog extends StatexWidget<_SubRouterDialogController> {
  SubRouterDialog(this.title,
      {Function()? okCallback,
      Function()? cancelCallback,
      Color? color,
      Key? key})
      : super(
          () => _SubRouterDialogController(
            okCallback: okCallback,
            cancelCallback: cancelCallback,
            color: color,
          ),
        );

  final String title;

  @override
  Widget buildWidget(BuildContext context) {
    final color = controller.color ?? Theme.of(context).primaryColor;
    final materialColor = MaterialStateProperty.all<Color>(color);
    return Material(
      child: Wrap(
        children: [
          Center(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .copyWith(color: color),
                  ),
                  16.h,
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: materialColor,
                    ),
                    onPressed: controller.okCallback,
                    child: Text('Go there'),
                  ),
                  8.h,
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: materialColor,
                    ),
                    onPressed: controller.cancelCallback,
                    child: Text('No, cancel!'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
    ;
  }
}

class _SubRouterDialogController extends StatexController {
  _SubRouterDialogController(
      {this.okCallback, this.cancelCallback, this.color});

  final Function()? okCallback;
  final Function()? cancelCallback;
  final Color? color;

  @override
  void onWidgetInitState() {
    super.onWidgetInitState();
    debugPrint('$now: _SubRouterDialogController.onWidgetInitState');
  }

  @override
  void onWidgetDisposed() {
    debugPrint('$now: _SubRouterDialogController.onWidgetDisposed');
    super.onWidgetDisposed();
  }

  @override
  void onInit() {
    super.onInit();
    debugPrint('$now: _SubRouterDialogController.onInit');
  }

  @override
  void onReady() {
    super.onReady();
    debugPrint('$now: _SubRouterDialogController.onReady');
  }

  @override
  void onClose() {
    debugPrint('$now: _SubRouterDialogController.onClose');
    super.onClose();
  }
}
