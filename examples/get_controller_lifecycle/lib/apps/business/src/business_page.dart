import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared/shared.dart';
import 'package:with_getx/with_getx.dart';

/// [BusinessPage] will be linked to [_BusinessPageController] type instance
/// directly.
class BusinessPage extends StatexWidget<_BusinessPageController> {
  BusinessPage() : super(() => _BusinessPageController(), tag: '_\$myTag') {
    debugPrint('$now: BusinessPage.BusinessPage');
  }

  @override
  Widget buildWidget(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            runtimeType.toString(),
            style: Theme.of(context).textTheme.headline5,
          ),
          16.h,
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Timer REINITIALIZES every time\n'
              'when initializes widget and controller',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
          Obx(() => Text(
                '${controller.timerTitle$}',
                style: Theme.of(context).textTheme.headline4,
              )),
          // _TestWidget(),
        ],
      ),
    );
  }
}

class _BusinessPageController extends StatexController {
  _BusinessPageController() {
    debugPrint('$now: _BusinessPageController._BusinessPageController');
  }

  final _timerTitle = 1.obs;

  get timerTitle$ => _timerTitle.value;

  late Timer _timer;

  @override
  void onWidgetInitState() {
    super.onWidgetInitState();
    debugPrint('$now: _BusinessPageController.onWidgetInitState');
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      _timerTitle.value++;
    });
  }

  @override
  void onWidgetDisposed() {
    debugPrint('$now: _BusinessPageController.onWidgetDisposed');
    super.onWidgetDisposed();
  }

  @override
  void onInit() {
    super.onInit();
    debugPrint('$now: _BusinessPageController.onInit');
  }

  @override
  void onReady() {
    super.onReady();
    debugPrint('$now: _BusinessPageController.onReady');
  }

  @override
  void onClose() {
    debugPrint('$now: _BusinessPageController.onClose');
    _timer.cancel();
    super.onClose();
  }
}
