import 'dart:io';
import 'dart:math';
import 'package:path/path.dart' as path;

///
String testDelimiter([int length = 80, bool time = true]) =>
    '-'.repeat(length) + (time ? '\n$now' : '\n');

///
extension StringTyper on String {
  /// repeating string [cnt] times
  String repeat([int cnt = 2]) {
    return List.generate(cnt, (i) => this).join();
  }
}

final random = Random();
String tempPath =
    path.join(Directory.current.path, '.dart_tool', 'test', 'tmp');

Future<Directory> getTempDir() async {
  final name = random.nextInt(pow(2, 32) as int);
  final dir = Directory(path.join(tempPath, '${name}_tmp'));
  if (dir.existsSync()) {
    await dir.delete(recursive: true);
  }
  await dir.create(recursive: true);
  return dir;
}

/// Delay in microseconds
Future delayMicro(int value) async =>
    Future<int>.delayed(Duration(microseconds: value));

/// Delay in milliseconds
Future<int> delayMilli(int value) async =>
    Future<int>.delayed(Duration(milliseconds: value), () => Future.value(1));

/// Delay in seconds
Future delaySec(int value) async =>
    Future<int>.delayed(Duration(seconds: value));

/// Shortcut for [DateTime.now]
DateTime get now => DateTime.now();
