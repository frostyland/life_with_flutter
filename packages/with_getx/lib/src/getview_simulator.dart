import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

/// Работает аналогично GetView в Get (https://pub.dev/packages/get)
/// (Симулятор, Sim), только добавляет для удобства [tag]
/// в параметры конструктора
///
abstract class GetViewSim<T> extends StatelessWidget {
  const GetViewSim({this.tag, Key? key}) : super(key: key);

  final String? tag;

  T get controller => GetInstance().find<T>(tag: tag)!;

  @override
  Widget build(BuildContext context);
}
