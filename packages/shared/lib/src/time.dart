/// Shortcut for [DateTime.now]
DateTime get now => DateTime.now();
